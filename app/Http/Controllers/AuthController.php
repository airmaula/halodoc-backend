<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    function __construct(User $user) {
        $this->user = $user;
    }

    public function register(Request $request) {

        // validate inputs
        $request->validate([
            'username' => 'required|unique:users,username',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{8,}$/',
        ], [
            'password.regex' => 'Password minimum of eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.',
        ]);

        // define params
        $token = hash('sha256', $request->username);
        $params = [
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'remember_token' => $token,
        ];

        // insert user
        $user = $this->user->create($params);

        // response success
        return response()->json([
            'message' => 'Register success',
            'data' => [
                'user' => $user,
                'token' => $token,
            ]
        ], 200);

    }

    public function login(Request $request) {

        // validate inputs
        $request->validate([
           'username' => 'required',
           'password' => 'required|min:8',
        ]);

        // define credentials
        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        // check credentials
        if (auth()->attempt($credentials)) {

            // get user
            $user = $this->user->where('username', $request->username)->first();

            // update token
            $token = hash('sha256', $user->username);
            $user->update([
               'remember_token' => $token,
            ]);

            // response success
            return response()->json([
                'message' => 'Login success',
                'data' => [
                    'user' => $user,
                    'token' => $token,
                ],
            ], 200);

        }

        // response failed
        return response()->json([
            'message' => 'Login failed',
            'errors' => [
                'username' => ['Username not found or password is wrong'],
            ]
        ], 422);

    }

    public function logout(Request $request) {
        // define user
        $user = $this->user->where('remember_token', $request->bearerToken())->first();

        // remove remember token
        $user->update(['remember_token' => NULL]);

        // response success
        return response()->json([
            'message' => 'Logout success',
        ], 200);

    }


}
