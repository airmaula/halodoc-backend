<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    function __construct(User $user, Order $order, Product $product) {
        $this->user = $user;
        $this->order = $order;
        $this->product = $product;
    }

    public function index(Request $request) {

        // define user
        $user = $this->user->where('remember_token', $request->bearerToken())->first();

        // get all orders created by user
        $orders = $user->orders()->with(['order_details.product'])->orderBy('id', 'desc')->get();

        // response success
        return response()->json([
            'message' => 'Get data successful',
            'data' => [
                'order' => $orders,
            ]
        ]);

    }

    public function store(Request $request) {

        // define user
        $user = $this->user->where('remember_token', $request->bearerToken())->first();

        // validate inputs
        $request->validate([
            'customer_name' => 'required',
            'order_details' => 'required|array',
        ], [
            'order_details.required' => 'Order details must have at least 1 product',
        ]);

        // define params
        $params = [
            'customer_name' => $request->customer_name,
            'date' => Carbon::now(),
            'created_by' => $user->id,
        ];

        foreach ($request->order_details as $detail) {
            $detail = (object)$detail;
            if (!$detail->product_id) {
                return response()->json([
                    'message' => 'There something error',
                    'errors' => ['product' => ['Product must be selected.']],
                ], 422);
            }
        }

        // insert order
        $order = $this->order->create($params);

        foreach ($request->order_details as $detail) {
            $detail = (object)$detail;
            $product = $this->product->find($detail->product_id);
            $order->order_details()->create([
                'product_id' => $product->id,
                'qty' => $detail->qty,
                'total' => $product->price * $detail->qty,
            ]);
        }

        // response success
        return response()->json([
            'message' => 'Order created successful',
            'data' => [
                'order' => $order,
            ]
        ], 200);
    }

    public function update(Request $request, Order $order) {

        // define user
        $user = $this->user->where('remember_token', $request->bearerToken())->first();

        // validate inputs
        $request->validate([
            'customer_name' => 'required',
            'order_details' => 'required|array|min:1',
        ], [
            'order_details.required' => 'Order details must have at least 1 product',
        ]);

        // define params
        $params = [
            'customer_name' => $request->customer_name,
        ];

        foreach ($request->order_details as $detail) {
            $detail = (object)$detail;
            if (!$detail->product_id) {
                return response()->json([
                    'message' => 'There something error',
                    'errors' => ['product' => ['Product must be selected.']],
                ], 422);
            }
        }

        // update order
        $order->update($params);

        foreach ($order->order_details as $order_detail) {
            $order_detail->delete();
        }
        foreach ($request->order_details as $detail) {
            $detail = (object)$detail;
            $product = $this->product->find($detail->product_id);
            $order->order_details()->create([
                'product_id' => $product->id,
                'qty' => $detail->qty,
                'total' => $product->price * $detail->qty,
            ]);
        }

        // response success
        return response()->json([
            'message' => 'Order updated successful',
            'data' => [
                'order' => $order,
            ]
        ], 200);
    }

}
