<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    function __construct(Product $product) {
        $this->product = $product;
    }
    public function index() {

        // get all products
        $products = $this->product->orderBy('name', 'asc')->get();

        // response success
        return response()->json([
            'message' => 'Get data successful',
            'data' => [
                'product' => $products,
            ]
        ]);

    }
}
