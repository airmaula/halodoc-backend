<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['total_header'];

    public function order_details() {
        return $this->hasMany(OrderDetail::class);
    }
    public function getTotalHeaderAttribute() {
        return collect($this->order_details)->reduce(function ($carry, $item) {
            return $carry + $item->total;
        }, 0);
    }
}
