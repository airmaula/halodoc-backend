<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $appends = ['total'];
    protected $guarded = [];

    public function getTotalAttribute() {
        return $this->qty * $this->product()->first()->price;
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }



}
