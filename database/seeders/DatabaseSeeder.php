<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');

        foreach (range(1, 5) as $i) {
            User::create([
               'username' => 'user' . $i,
               'password' => bcrypt('#Pass123'),
            ]);
        }

        $products = [
            ['code' => 'A0001', 'name' => 'Macbook Pro M1', 'price' => 21500000],
            ['code' => 'A0002', 'name' => 'iPhone 13', 'price' => 16000000],
            ['code' => 'A0003', 'name' => 'Airpods Pro', 'price' => 4500000],
            ['code' => 'A0004', 'name' => 'Macbook Air M1', 'price' => 16000000],
            ['code' => 'A0005', 'name' => 'Airpods Gen 2', 'price' => 2150000],
            ['code' => 'A0006', 'name' => 'iPhone 13 Pro', 'price' => 18000000],
            ['code' => 'A0007', 'name' => 'iPhone 13 Pro Max', 'price' => 22000000],
            ['code' => 'A0008', 'name' => 'iPad Mini 6', 'price' => 8700000],
            ['code' => 'A0009', 'name' => 'iPad Air', 'price' => 10500000],
            ['code' => 'A0010', 'name' => 'iPad Pro', 'price' => 12000000],
        ];

        foreach ($products as $product) {
            Product::create($product);
        }

        $product_ids = Product::all()->map(function($product) {
           return $product->id;
        });
        foreach (range(1, 5) as $x) {
            $order = Order::create([
                'created_by' => 1,
                'customer_name' => $faker->name,
                'date' => Carbon::now(),
            ]);
            foreach (range(1, 2) as $y) {
                $qty = $faker->numberBetween(1, 10);
                $product_id = $faker->randomElement($product_ids);
                OrderDetail::create([
                    'order_id' => $order->id,
                    'product_id' => $product_id,
                    'qty' => $qty,
                    'total' => Product::find($product_id)->price * $qty,
                ]);
            }
        }
    }
}
