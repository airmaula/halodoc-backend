<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1'], function() {

    Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register']);

    Route::group(['middleware' => 'user'], function() {
        Route::post('/logout', [\App\Http\Controllers\AuthController::class, 'logout']);
        Route::apiResource('/product', \App\Http\Controllers\ProductController::class);
        Route::apiResource('/order', \App\Http\Controllers\OrderController::class);
    });

});
